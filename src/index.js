const messages = [
    "Henry",
    "Vivi",
    "Tamy",
    "Jimmy",
    "Marciana",
    "Nelson",
    "Gonzalo",
    "Fabricio"

];

const randomMsg = () =>{
    const message = messages[Math.floor(Math.random() *messages.length)];
    console.log(message);
}

module.exports = {randomMsg}